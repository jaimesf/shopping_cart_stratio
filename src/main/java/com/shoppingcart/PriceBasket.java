package com.shoppingcart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.shoppingcart.model.ShoppingCart;
import com.shoppingcart.service.PrinterService;
import com.shoppingcart.service.ShoppingCartService;

/**
 * Print a shopping cart resume with product names as arguments
 * @author jaimesf
 *
 */
@SpringBootApplication
public class PriceBasket implements CommandLineRunner{

	private ShoppingCartService shoppingCartService;
	private PrinterService printerService;
	
	@Autowired
	public PriceBasket(ShoppingCartService shoppingCartService, PrinterService printerService) {
		this.shoppingCartService = shoppingCartService;
		this.printerService = printerService;
	}
	
	public static void main(String[] args) {
	    SpringApplication.run(PriceBasket.class, args);
	  }

	@Override
	public void run(String... args) throws Exception {
		
		if(args.length == 0) {
			System.out.println("No items to add");
		}else {
			ShoppingCart shoppingCart = shoppingCartService.getShoppingCartFromArguments(args);
			
			if(shoppingCart.getItems().size() == 0) {
				System.out.println("Shopping cart is empty");
			}else {
				printerService.printShoppingCart(shoppingCart);
			}
		}

	}
	
}
