package com.shoppingcart.service;

import com.shoppingcart.model.ShoppingCart;

/**
 * Print shopping cart service
 * @author jaimesf
 *
 */
public interface PrinterService {

	/**
	 * Prints a shopping cart to console
	 * @param shoppingCart
	 */
	public void printShoppingCart(ShoppingCart shoppingCart);

}
