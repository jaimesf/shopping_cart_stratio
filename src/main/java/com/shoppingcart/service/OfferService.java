package com.shoppingcart.service;

import java.util.List;

import com.shoppingcart.model.ItemLine;
import com.shoppingcart.model.Offer;

/**
 * Offers service
 * @author jaimesf
 *
 */
public interface OfferService {

	/**
	 * Get offer for an item
	 * @param itemOffer item to get an offer
	 * @param items items on shopping cart
	 * @return
	 */
	public Offer getOfferItemForItemLines(ItemLine itemOffer, List<ItemLine> items);
}
