package com.shoppingcart.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shoppingcart.model.ItemLine;
import com.shoppingcart.model.Product;
import com.shoppingcart.model.ShoppingCart;
import com.shoppingcart.repository.ProductRepository;
import com.shoppingcart.service.OfferService;
import com.shoppingcart.service.ShoppingCartService;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService{

	private ProductRepository productRepository;
	private OfferService offerService;
	
	
	@Autowired
	public ShoppingCartServiceImpl(ProductRepository productRepository, OfferService offerService) {
		this.productRepository = productRepository;
		this.offerService = offerService;
	}
	
	@Override
	public ShoppingCart getShoppingCartFromArguments(String... args) {

		ShoppingCart shoppingCart = new ShoppingCart();
		
		for(String productName : args) {
			ItemLine item = getItemLineFromShoppingCart(productName, shoppingCart);
			if(item==null) {
				Optional<Product> product = productRepository.findByName(productName);
				if(product.isPresent()) {
					shoppingCart.addNewProduct(product.get());
				}
			}else {
				item.addQuantity();
			}
			
		}
		
		for(ItemLine itemLine :shoppingCart.getItems()) {
			itemLine.setOffer(offerService.getOfferItemForItemLines(itemLine, shoppingCart.getItems()));
		}
		
		shoppingCart.setTaxes(getTotalTaxesFromShoppingCart(shoppingCart));
		
		return shoppingCart;
	}

	private ItemLine getItemLineFromShoppingCart(String productName, ShoppingCart shoppingCart) {
		for(ItemLine item : shoppingCart.getItems()){
			if(item.getProduct().getName().equals(productName)) {
				return item;
			}
		}
		return null;
	}
	
	private Map<Integer,Double> getTotalTaxesFromShoppingCart(ShoppingCart shoppingCart){
		Map<Integer, Double> taxes = new HashMap<Integer, Double>();
		
		for(ItemLine item : shoppingCart.getItems()) {
			Double totalTaxType = taxes.get(item.getProduct().getTax());
			if(totalTaxType==null) {
				taxes.put(item.getProduct().getTax(), getTaxFromItem(item));
			}else {
				totalTaxType = totalTaxType.doubleValue()+getTaxFromItem(item);
			}
		}
		
		return taxes;
		
	}
	
	private Double getTaxFromItem(ItemLine itemLine) {
		return itemLine.getTotalAmountItem()*((double)itemLine.getProduct().getTax()/100);
	}

}
