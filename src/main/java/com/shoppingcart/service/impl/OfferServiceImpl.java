package com.shoppingcart.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.shoppingcart.model.ItemLine;
import com.shoppingcart.model.Offer;
import com.shoppingcart.model.Product;
import com.shoppingcart.service.OfferService;

@Service
public class OfferServiceImpl implements OfferService {

	@Override
	public Offer getOfferItemForItemLines(ItemLine itemOffer, List<ItemLine> items) {
		
		Offer result = null;
		
		for(ItemLine itemLine : items) {
			if(itemOffer.equals(itemLine)) {
				Product product = itemLine.getProduct();
				if(product.getName().equalsIgnoreCase("Apples")) {
					double totalDiscount = itemLine.getQuantity()*product.getPrice()*0.10;
					result = new Offer("Apples 10% off", totalDiscount);
				}else if(product.getName().equalsIgnoreCase("Bread")) {
					ItemLine soups = items.stream()                
			                .filter(item -> (item.getProduct().getName().equalsIgnoreCase("Soup") && item.getQuantity() >= 2))     
			                .findAny()
			                .orElse(null);
					if(soups!=null) {
						int totalLoafBreastDiscount = soups.getQuantity()/2;
						
						double totalDiscountForBread = 0.0;
						for(int i = 0 ; i < totalLoafBreastDiscount && i < itemLine.getQuantity() ; i++) {
							totalDiscountForBread += product.getPrice()/2;
						}
						result = new Offer("Loaf bread 50% with 2 tins of soup off", totalDiscountForBread);
					}
					
				}
			}
			
		}
		
		return result;
	}

}
