package com.shoppingcart.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.springframework.stereotype.Service;

import com.shoppingcart.model.ItemLine;
import com.shoppingcart.model.Offer;
import com.shoppingcart.model.ShoppingCart;
import com.shoppingcart.service.PrinterService;

@Service
public class PrinterServiceImpl implements PrinterService{

	@Override
	public void printShoppingCart(ShoppingCart shoppingCart) {
		
		DecimalFormat df = new DecimalFormat("0.00");

		double subtotal = 0.0;
		double discounts = 0.0;
		double base = 0.0;
		double taxes = 0.0;
		double total = 0.0;
		StringBuilder out = new StringBuilder();
		
		StringBuilder offersBuilder = new StringBuilder();
		for(ItemLine item : shoppingCart.getItems()) {
			
			subtotal += round(item.getProduct().getPrice()*item.getQuantity());
			
			Offer itemOffer = item.getOffer();
			if(itemOffer != null) {
				offersBuilder.append("\t"+itemOffer.getName()+": -"+df.format(round(itemOffer.getDiscount()))+"€\n");
				discounts += round(itemOffer.getDiscount());
			}
			
		}
		
		subtotal = round(subtotal);
		base = round(subtotal-discounts);
		StringBuilder taxesBuilder = new StringBuilder();
		for(Integer tax : shoppingCart.getTaxes().keySet()) {
			Double taxValue = round(shoppingCart.getTaxes().get(tax));
			taxes += taxValue;
			taxesBuilder.append("\tTaxes "+tax+"%: "+df.format(taxValue)+"€\n");
		}
		taxes = round(taxes);
		total = round(subtotal-discounts+taxes);

		
		out.append("\tSubtotal: "+df.format(subtotal)+"€\n");
		out.append(offersBuilder);
		out.append("\tBase: "+df.format(base)+"€\n");
		out.append(taxesBuilder);
		out.append("\tTotal: "+df.format(total)+"€");

		
		System.out.println(out);
		
	}
	
	private double round(double value) {
	    BigDecimal bd = new BigDecimal(Double.toString(value));
	    bd = bd.setScale(2, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}

}
