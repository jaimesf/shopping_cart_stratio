package com.shoppingcart.service;

import com.shoppingcart.model.ShoppingCart;

/**
 * Shopping cart service
 * @author jaimesf
 *
 */
public interface ShoppingCartService {

	/**
	 * Get a shopping cart based on console arguments
	 * @param args product names
	 * @return
	 */
	public ShoppingCart getShoppingCartFromArguments(String... args);
}
