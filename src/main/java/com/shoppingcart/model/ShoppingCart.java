package com.shoppingcart.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Shopping cart
 * @author jaimesf
 *
 */
public class ShoppingCart {

	private List<ItemLine> items = new ArrayList<ItemLine>();
	private Map<Integer, Double> taxes = new HashMap<Integer,Double>();
	
	public List<ItemLine> getItems() {
		return items;
	}
	public void setItems(List<ItemLine> items) {
		this.items = items;
	}
	public Map<Integer, Double> getTaxes() {
		return taxes;
	}
	public void setTaxes(Map<Integer, Double> taxes) {
		this.taxes = taxes;
	}
	
	public void addNewProduct(Product product) {
		items.add(new ItemLine(product, 1));
	}
	
}
