package com.shoppingcart.model;

/**
 * A item line of shopping cart
 * @author jaimesf
 *
 */
public class ItemLine {

	private Product product;
	private Offer offer;
	private Integer quantity;
	
	/**
	 * Create an item line with product and quantity
	 * @param product
	 * @param quantity
	 */
	public ItemLine(Product product, Integer quantity) {
		super();
		this.product = product;
		this.quantity = quantity;
	}
	
	/**
	 * Adds quantity to this item line
	 */
	public void addQuantity() {
		this.quantity++;
	}
	
	/**
	 * Calculate total amount for this item line
	 * @return
	 */
	public Double getTotalAmountItem() {
		Double offerDiscount = 0.0;
		if(this.offer!=null) {
			offerDiscount = this.offer.getDiscount();
		}
		return ((product.getPrice()*quantity)-offerDiscount);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemLine other = (ItemLine) obj;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		return true;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	
}
