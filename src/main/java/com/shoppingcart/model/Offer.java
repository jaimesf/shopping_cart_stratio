package com.shoppingcart.model;

/**
 * Offer associated to an item line
 * @author jaimesf
 *
 */
public class Offer {

	private String name;
	
	private Double discount;

	public Offer(String name, Double discount) {
		super();
		this.name = name;
		this.discount = discount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	
}
