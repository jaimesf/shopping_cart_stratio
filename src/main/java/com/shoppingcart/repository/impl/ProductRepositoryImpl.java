package com.shoppingcart.repository.impl;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.shoppingcart.model.Product;
import com.shoppingcart.repository.ProductRepository;

@Repository
public class ProductRepositoryImpl implements ProductRepository{

	@Override
	public Optional<Product> findByName(String name) {
		Optional<Product> result = null;
		if(name.equalsIgnoreCase("Soup")) {
			Product soup = createSoupProduct();
			result = Optional.of(soup);
		}else if(name.equalsIgnoreCase("Milk")) {
			Product milk = createMilkProduct();
			result = Optional.of(milk);
		}else if(name.equalsIgnoreCase("Bread")) {
			Product bread = createBreadProduct();
			result = Optional.of(bread);
		}else if(name.equalsIgnoreCase("Apples")) {
			Product apple = createApplesProduct();
			result = Optional.of(apple);
		}else {
			result = Optional.empty();
		}
		return result;
	}

	private Product createApplesProduct() {
		Product result = new Product("Apples", new Double(1.00), 5);
		return result;
	}

	private Product createBreadProduct() {
		Product result = new Product("Bread", new Double(0.80), 2);
		return result;
	}

	private Product createMilkProduct() {
		Product result = new Product("Milk", new Double(1.30), 2);
		return result;
	}

	private Product createSoupProduct() {
		Product result = new Product("Soup", new Double(0.65), 5);
		return result;
	}
	
	

}
