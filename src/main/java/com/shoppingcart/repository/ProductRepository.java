package com.shoppingcart.repository;

import java.util.Optional;

import com.shoppingcart.model.Product;

/**
 * Product repository
 * @author jaimesf
 *
 */
public interface ProductRepository {

	/**
	 * Get a product by its name
	 * @param name name of the product
	 * @return
	 */
	public Optional<Product> findByName(String name);
	
}
