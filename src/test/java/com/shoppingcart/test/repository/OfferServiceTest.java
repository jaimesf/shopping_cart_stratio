package com.shoppingcart.test.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.shoppingcart.model.ItemLine;
import com.shoppingcart.model.Offer;
import com.shoppingcart.model.Product;
import com.shoppingcart.service.OfferService;
import com.shoppingcart.service.impl.OfferServiceImpl;

public class OfferServiceTest {

	@Test
	public void test1ApplesDiscount() {
		List<ItemLine> items = createItemsLineApplesDiscount(1);
		OfferService offerService = new OfferServiceImpl();
		
		ItemLine applesItemLine = createApplesItemLine(1);
		
		Offer applesOffer = offerService.getOfferItemForItemLines(applesItemLine, items);
		
		assertNotNull(applesOffer);
		
		assertEquals("1 bag of apples has 10% discount", 0.10, applesOffer.getDiscount(), 0.0);
	}
	
	@Test
	public void test2ApplesDiscount() {
		List<ItemLine> items = createItemsLineApplesDiscount(2);
		OfferService offerService = new OfferServiceImpl();
		
		ItemLine applesItemLine = createApplesItemLine(1);
		
		Offer applesOffer = offerService.getOfferItemForItemLines(applesItemLine, items);
		
		assertNotNull(applesOffer);
		
		assertEquals("2 bag of apples has 10% discount", 0.20, applesOffer.getDiscount(), 0.0);
	}
	
	@Test
	public void test2SoupsAnd1LoafBreadDiscount1BreadLoaf() {
		List<ItemLine> items = createItemsLineBreadDiscount(2, 1);
		
		OfferService offerService = new OfferServiceImpl();
		
		ItemLine breadItemLine = createBreadItemLine(1);
		
		Offer breadOffer = offerService.getOfferItemForItemLines(breadItemLine, items);
		
		assertNotNull(breadOffer);
		
		assertEquals("1 loaf of bread with 50% off", 0.40, breadOffer.getDiscount(), 0.0);
	}
	
	@Test
	public void test3SoupsAnd1LoafBreadDiscount1BreadLoaf() {
		List<ItemLine> items = createItemsLineBreadDiscount(3, 1);
		
		OfferService offerService = new OfferServiceImpl();
		
		ItemLine breadItemLine = createBreadItemLine(1);	
		
		Offer breadOffer = offerService.getOfferItemForItemLines(breadItemLine, items);
		
		assertNotNull(breadOffer);
		
		assertEquals("1 loaf of bread with 50% off", 0.40, breadOffer.getDiscount(), 0.0);
	}
	
	@Test
	public void test4SoupsAnd1LoafBreadDiscount1BreadLoaf() {
		List<ItemLine> items = createItemsLineBreadDiscount(4, 1);
		
		OfferService offerService = new OfferServiceImpl();
		
		ItemLine breadItemLine = createBreadItemLine(1);
		
		Offer breadOffer = offerService.getOfferItemForItemLines(breadItemLine, items);
		
		assertNotNull(breadOffer);
		
		assertEquals("1 loaf of bread with 50% off", 0.40, breadOffer.getDiscount(), 0.0);
	}
	
	@Test
	public void test4SoupsAnd2LoafBreadDiscount2BreadLoaf() {
		List<ItemLine> items = createItemsLineBreadDiscount(4, 2);
		
		OfferService offerService = new OfferServiceImpl();
		
		ItemLine breadItemLine = createBreadItemLine(1);
		
		Offer breadOffer = offerService.getOfferItemForItemLines(breadItemLine, items);
		
		assertNotNull(breadOffer);
		
		assertEquals("2 loafs of bread with 50% off", 0.80, breadOffer.getDiscount(), 0.0);
	}
	
	private List<ItemLine> createItemsLineApplesDiscount(int apples){
		List<ItemLine> result = new ArrayList<ItemLine>();
		
		ItemLine itemApples = createApplesItemLine(apples);

		result.add(itemApples);
		
		return result;
	}
	
	private List<ItemLine> createItemsLineBreadDiscount(int soups, int loafBread){
		List<ItemLine> result = new ArrayList<ItemLine>();
		
		ItemLine itemSoup = createSoupItemLine(soups);
		ItemLine itemBread = createBreadItemLine(loafBread);
		
		result.add(itemSoup);
		result.add(itemBread);
		
		return result;
	}
	
	private ItemLine createSoupItemLine(int soups) {
		Product soupProduct = new Product("Soup", 0.65, 5);
		return new ItemLine(soupProduct, soups);
	}
	
	private ItemLine createBreadItemLine(int loadBreads) {
		Product breadProduct = new Product("Bread", 0.80, 2);
		return new ItemLine(breadProduct, loadBreads);
	}
	
	private ItemLine createApplesItemLine(int apples) {
		Product applesProduct = new Product("Apples", 1.00, 5);
		return new ItemLine(applesProduct, apples);
	}
	
}
