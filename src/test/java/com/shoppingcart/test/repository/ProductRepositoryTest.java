package com.shoppingcart.test.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.Test;

import com.shoppingcart.model.Product;
import com.shoppingcart.repository.ProductRepository;
import com.shoppingcart.repository.impl.ProductRepositoryImpl;

public class ProductRepositoryTest {

	@Test
	public void testApplesExist() {
		assertTrue("Product Apples exist", getProduct("Apples").isPresent());
	}
	
	@Test
	public void testSoupExist() {
		
		assertTrue("Product Soup exist", getProduct("Soup").isPresent());
	}
	
	@Test
	public void testMilkExist() {
		assertTrue("Product Milk exist", getProduct("Milk").isPresent());
	}
	
	@Test
	public void testBreadExist() {
		assertTrue("Product Bread exist", getProduct("Bread").isPresent());
	}
	
	@Test
	public void testNewProductNotExist() {
		assertFalse("Product New product notexist", getProduct("NewProduct").isPresent());
	}
	
	@Test
	public void testMilkHasCorrectPrice() {
		
		assertEquals("Milk has the correct price", 1.30, getProduct("Milk").get().getPrice().doubleValue(), 0);
	}
	
	@Test
	public void testBreadHasCorrectPrice() {
		assertEquals("Bread has the correct price", 0.80, getProduct("Bread").get().getPrice().doubleValue(), 0);
	}
	
	@Test
	public void testSoupHasCorrectPrice() {
		assertEquals("Soup has the correct price", 0.65, getProduct("Soup").get().getPrice().doubleValue(), 0);
	}
	
	@Test
	public void testApplesHasCorrectPrice() {
		assertEquals("Apples has the correct price", 1.00, getProduct("Apples").get().getPrice().doubleValue(), 0);
	}
	
	@Test
	public void testMilkHasCorrectTax() {
		
		assertEquals("Milk has the correct tax", 2.00, getProduct("Milk").get().getTax().doubleValue(), 0);
	}
	
	@Test
	public void testBreadHasCorrectTax() {
		assertEquals("Bread has the correct tax", 2.00, getProduct("Bread").get().getTax().doubleValue(), 0);
	}
	
	@Test
	public void testSoupHasCorrectTax() {
		assertEquals("Soup has the correct tax", 5.00, getProduct("Soup").get().getTax().doubleValue(), 0);
	}
	
	@Test
	public void testApplesHasCorrectTax() {
		assertEquals("Apples has the correct tax", 5.00, getProduct("Apples").get().getTax().doubleValue(), 0);
	}
	
	private Optional<Product> getProduct(String name){
		ProductRepository productRepository = new ProductRepositoryImpl();
		
		return productRepository.findByName(name);
	}

		
}
