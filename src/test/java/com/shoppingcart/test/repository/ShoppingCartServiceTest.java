package com.shoppingcart.test.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;

import com.shoppingcart.model.ItemLine;
import com.shoppingcart.model.Offer;
import com.shoppingcart.model.Product;
import com.shoppingcart.model.ShoppingCart;
import com.shoppingcart.repository.ProductRepository;
import com.shoppingcart.repository.impl.ProductRepositoryImpl;
import com.shoppingcart.service.OfferService;
import com.shoppingcart.service.ShoppingCartService;
import com.shoppingcart.service.impl.OfferServiceImpl;
import com.shoppingcart.service.impl.ShoppingCartServiceImpl;

public class ShoppingCartServiceTest {

	@Test
	public void testEmptyShoppingCartWithInexistentProduct() {
		
		ShoppingCartService shoppingCartService = new ShoppingCartServiceImpl(mockProductRepositoryEmptyWithInexistentProduct(), mockOfferServiceNoOffers());
		
		ShoppingCart shoppingCart = shoppingCartService.getShoppingCartFromArguments("NoProduct");
		
		assertTrue(shoppingCart.getItems().isEmpty());
		
	}
	
	@Test
	public void testNotEmptyShoppingCartWithExistentProduct() {
		
		ShoppingCartService shoppingCartService = new ShoppingCartServiceImpl(mockProductRepositoryWithSoupProduct(), mockOfferServiceNoOffers());
		
		ShoppingCart shoppingCart = shoppingCartService.getShoppingCartFromArguments("Soup");
		
		assertFalse(shoppingCart.getItems().isEmpty());
		
	}
	
	@Test
	public void testShoppingCartWith2SameExistentProduct() {
		
		ShoppingCartService shoppingCartService = new ShoppingCartServiceImpl(mockProductRepositoryWithSoupProduct(), mockOfferServiceNoOffers());
		
		ShoppingCart shoppingCart = shoppingCartService.getShoppingCartFromArguments("Soup","Soup");
		
		assertFalse(shoppingCart.getItems().isEmpty());
		assertEquals("Shopping cart has 2 items of soup",2L,shoppingCart.getItems().get(0).getQuantity().longValue());
		
	}
	
	@Test
	public void testShoppingCartWithItemOffer() {
		
		ShoppingCartService shoppingCartService = new ShoppingCartServiceImpl(mockProductRepositoryWithApplesProduct(), mockOfferServiceApplesOffer());
		
		ShoppingCart shoppingCart = shoppingCartService.getShoppingCartFromArguments("Apples");
		
		assertFalse(shoppingCart.getItems().isEmpty());
		assertTrue("Shopping cart has 1 item with offer",shoppingCart.getItems().get(0).getOffer()!=null);
		
	}
	
	
	
	private ProductRepository mockProductRepositoryEmptyWithInexistentProduct() {
		ProductRepository productRepository = mock(ProductRepositoryImpl.class);
		when(productRepository.findByName("NoProduct")).thenReturn(Optional.empty());
		
		return productRepository;
	}
	
	private ProductRepository mockProductRepositoryWithSoupProduct() {
		ProductRepository productRepository = mock(ProductRepositoryImpl.class);
		when(productRepository.findByName("Soup")).thenReturn(Optional.of(createSoupProduct()));
		
		return productRepository;
	}
	
	private ProductRepository mockProductRepositoryWithApplesProduct() {
		ProductRepository productRepository = mock(ProductRepositoryImpl.class);
		when(productRepository.findByName("Apples")).thenReturn(Optional.of(new Product("Apples", 1.00, 5)));
		
		return productRepository;
	}
	
	private OfferService mockOfferServiceNoOffers() {
		OfferService offerService = mock(OfferServiceImpl.class);
		
		return offerService;
	}
	
	private OfferService mockOfferServiceApplesOffer() {
		OfferService offerService = mock(OfferServiceImpl.class);
		when(offerService.getOfferItemForItemLines(createApplesItemLine(1), createItemsLineApplesDiscount(2))).thenReturn(getApplesOffer());
		
		return offerService;
	}
	
	private Product createSoupProduct() {
		return new Product("Soup", 0.65, 5);
	}
	
	private List<ItemLine> createItemsLineApplesDiscount(int apples){
		List<ItemLine> result = new ArrayList<ItemLine>();
		
		ItemLine itemApples = createApplesItemLine(apples);

		result.add(itemApples);
		
		return result;
	}
	
	private ItemLine createApplesItemLine(int apples) {
		Product applesProduct = new Product("Apples", 1.00, 5);
		return new ItemLine(applesProduct, apples);
	}
	
	private Offer getApplesOffer() {
		return new Offer("Apples discount",0.10);
	}
}
